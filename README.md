# Guild Wars 2 Bag Layout Visualizer

**Check it out here: <https://scy.codeberg.page/gw2-bag-layout-visualizer/@main/>**

## Links and Stuff

This project’s official repository is at <https://codeberg.org/scy/gw2-bag-layout-visualizer>.

Contributions are welcome, check the [issues](https://codeberg.org/scy/gw2-bag-layout-visualizer/issues) for things that might be helpful.
Before investing time in implementing a large new feature, maybe create an issue so that we can talk about it.

## Legal Notice

If you couldn’t already tell, this project is in no way affiliated with Guild Wars 2, ArenaNet or NCSOFT.
Guild Wars 2 is © 2010–2022 ArenaNet, LLC.
All rights reserved.
Guild Wars, Guild Wars 2, ArenaNet, NCSOFT and all associated logos and designs are trademarks or registered trademarks of NCSOFT Corporation.
All other trademarks are the property of their respective owners.

This project itself is licensed under 2-clause BSD.
